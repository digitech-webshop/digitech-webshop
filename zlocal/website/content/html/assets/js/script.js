const chatBox = document.getElementById('chat-box');
const userInput = document.getElementById('user-input');

function appendMessage(message, sender) {
    const msgElement = document.createElement('div');
    msgElement.className = `message ${sender}`;
    msgElement.innerText = message;
    chatBox.appendChild(msgElement);
    chatBox.scrollTop = chatBox.scrollHeight;
}

function sendMessage() {
    const message = userInput.value.trim();
    if (message !== '') {
        appendMessage(message, 'user');
        fetch('https://digitech.inf2021d.m300.smartlearn.ch/backend', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: `user_input=${encodeURIComponent(message)}`,
        })
        .then(response => response.text())
        .then(data => appendMessage(data, 'bot'))
        .catch(error => console.error('Error:', error));
        userInput.value = '';
    }
}

userInput.addEventListener('keypress', function(event) {
    if (event.key === 'Enter') {
        event.preventDefault();
        sendMessage();
    }
});
