echo "------ Start creating virtual environment ------"
python3.9 -m venv .venv
source .venv/bin/activate
echo "------ Finished creating virtual environment ------"

echo "------ Start installing python libraries ------"
pip3 install llama-index-llms-gemini
pip3 install llama-index google-generativeai
pip3 install flask
pip3 install flask_cors
pip3 install jsonify
pip3 install grpc
python -m pip install grpcio
pip3 install chromadb
pip3 install pysqlite3-binary
echo "------ Finished installing python libraries ------"