from flask import Flask, render_template, request
from flask_cors import CORS
import os
import google.generativeai as genai
from llama_index import SimpleDirectoryReader

os.environ['GOOGLE_API_KEY'] = "AIzaSyDsKEPP5OK59PLkHxZjoG3hfhtgEGakdCQ"
genai.configure(api_key=os.environ['GOOGLE_API_KEY'])

model = genai.GenerativeModel('gemini-1.0-pro')

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes
app.config['TEMPLATES_AUTO_RELOAD'] = True  # Enable template auto-reloading

documents = SimpleDirectoryReader("./data").load_data()

@app.route("/backend", methods=["POST"])
def index():
    user_input = request.form.get("user_input")
    response_data = model.generate_content(user_input)
    return response_data.text
    
if __name__ == "__main__":
    app.run(debug=True)
