from flask import Flask, request, jsonify
from flask_cors import CORS
from llama_index.core import VectorStoreIndex, SimpleDirectoryReader
from llama_index.llms.gemini import Gemini
from llama_index.core import ServiceContext
from llama_index.core.storage.storage_context import StorageContext
import os
# import psycopg2

__import__('pysqlite3')
import sys
sys.modules['sqlite3'] = sys.modules.pop('pysqlite3')
import chromadb


app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

local_documents = SimpleDirectoryReader("./data").load_data()

# conn = psycopg2.connect(database="your_db_name", user="your_username", password="your_password", host="your_host", port="your_port")
# cur = conn.cursor()
# cur.execute("SELECT * FROM your_table_name;")
# postgres_data = cur.fetchall()

documents = local_documents #+ postgres_data

db = chromadb.PersistentClient(path="./embeddings/chroma_db")

# db = chromadb.PersistentDB(path="./embeddings/chroma_db")
chroma_collection = db.get_or_create_collection("quickstart")

os.environ['GOOGLE_API_KEY'] = "AIzaSyDsKEPP5OK59PLkHxZjoG3hfhtgEGakdCQ"
llm = Gemini(model_version="gemini-1.0-pro")
service_context = ServiceContext.from_defaults(llm=llm, chunk_size=8000, chunk_overlap=20, embed_model="local")

storage_context = StorageContext(chroma_collection=chroma_collection)
index = VectorStoreIndex.from_documents(documents, storage_context=storage_context, service_context=service_context)

@app.route('/api/query', methods=['POST'])
def query():
    user_input = request.json.get('user_input')

    if user_input:
        response = index.query(user_input)
        return jsonify({"response": response})
    else:
        return jsonify({"error": "User input is required."}), 400

if __name__ == "__main__":
    app.run(debug=True)
    