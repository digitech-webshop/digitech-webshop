from openai import OpenAI
from flask import Flask, request, jsonify
from flask_cors import CORS
import os
import json
 
# os.environ['OPENAI_API_KEY'] = "sk-proj-wa1nCNCwAeNp6BWe0bkyT3BlbkFJbUtRg16deyA6d8GYU7sG"
 
client = OpenAI(
    api_key="sk-proj-wa1nCNCwAeNp6BWe0bkyT3BlbkFJbUtRg16deyA6d8GYU7sG",
)
 
app = Flask(__name__)
CORS(app)
 
# Load product data from file
with open('./data/products.json', 'r') as file:
    product_data = json.load(file)
 
@app.route('/api/chatbot', methods=['POST'])
def chatbot():
    data = request.get_json()
    user_input = data.get('text')
 
    print(user_input)
 
    # Create a system message that includes product data
    system_message = {
        "role": "system",
        "content": f"You are a chatbot on an online shop which sells computer components. Here are the product details: {json.dumps(product_data)}"
    }
 
    completion = client.chat.completions.create(
        model="gpt-3.5-turbo-0125",
        messages=[
            system_message,
            {"role": "user", "content": user_input}
        ],
        max_tokens=50
    )
 
    print(completion.choices[0].message)
 
    # Extract content from the message
    response = completion.choices[0].message.content
 
    return jsonify(response), 200
 
if __name__ == '__main__':
    app.run(debug=True)