from flask import Flask, render_template, request
from flask_cors import CORS
import os
import google.generativeai as genai
import jsonify

os.environ['GRPC_DNS_RESOLVER'] = "native"
os.environ['GOOGLE_API_KEY'] = "AIzaSyDsKEPP5OK59PLkHxZjoG3hfhtgEGakdCQ"
genai.configure(api_key=os.environ['GOOGLE_API_KEY'])

model = genai.GenerativeModel('gemini-1.0-pro')

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes
app.config['TEMPLATES_AUTO_RELOAD'] = True  # Enable template auto-reloading

@app.route("/backend", methods=["POST"])
def index():
    user_input = request.form.get("user_input")
    response_data = model.generate_content(user_input)
    return render_template("index.html", response_data=response_data.text)
    
@app.route('/api/chatbot', methods=['POST'])
def chatbot():
    # user_message = request.json.get('message')  # Assuming JSON input
    # # Send user_message to the chatbot library/API and get the bot's response
    # bot_response = chatbot_library.get_response(user_message)
    user_input = request.json.get('message')  # Assuming JSON input
    response_data = model.generate_content(user_input)
    return jsonify({'response': response_data.text})

if __name__ == "__main__":
    app.run(debug=True)

# https://medium.com/@sreevedvp/building-a-chatbot-with-googles-gemini-api-and-fastapi-82285fb0e179
# https://www.youtube.com/watch?v=w73nrTquxm0
# https://www.youtube.com/watch?v=sf5MrM0AIiU
# https://codemaker2016.medium.com/build-your-own-chatgpt-using-google-gemini-api-1b079f6a8415
