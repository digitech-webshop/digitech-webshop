-- User Table
CREATE TABLE product (
  id SERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(255) NOT NULL,
  price NUMERIC(7,5),
  currency TEXT CHECK (currency IN ('USD', 'CHF', 'EUR'),
);
